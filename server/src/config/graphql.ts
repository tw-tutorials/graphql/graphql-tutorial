import { Options } from 'express-graphql';
import schema from '../schema/schema';

const options: Options = {
	schema: schema,
	graphiql: true
};

export default options;
