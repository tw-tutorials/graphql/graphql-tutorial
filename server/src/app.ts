import express, { Application, Request, Response } from 'express';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import cors from 'cors';
import mongoose from 'mongoose';

import expressGraphQL from 'express-graphql';
import gqlOptions from './config/graphql';

// Create a new express application
const app: Application = express();

// Connect to databse
mongoose.connect(
	'mongodb+srv://tobiaswaelde:O2USPjYnVflQP6fL@graphql-ohlz3.mongodb.net/test?retryWrites=true&w=majority',
	{ useUnifiedTopology: true, useNewUrlParser: true }
);
mongoose.connection.once('open', () => {
	console.log('connected to database');
});

// remove x-powered-by
app.disable('x-powered-by');

// Middleware
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// API routes
app.use('/gql', expressGraphQL(gqlOptions));

export default app;
