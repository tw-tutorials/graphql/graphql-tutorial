export interface IBook {
	id: string;
	name: string;
	genre: string;
	authorId: string;
}

export interface IAuthor {
	id: string;
	name: string;
	age: number;
}
