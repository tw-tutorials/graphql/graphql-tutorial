import app from '../app';
import debug from 'debug';
import http from 'http';

// Get port from environment and store in Express
const port = normalizePort(process.env.API_PORT || '3001'); // Use 3001 as fallback port
app.set('port', port);

// Create HTTP server
const server = http.createServer(app);

// Listen on provided port, on all network interfaces
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

// Normalize a port into a number, string or false
function normalizePort(value: any) {
	const port = parseInt(value, 10);

	// check if port is a number
	if (isNaN(port)) {
		// named pipe
		return value;
	}

	// check if port is valid number
	if (port >= 0) {
		return value;
	}

	// fallback
	return false;
}

// Event listener for HTTP server "error" event
function onError(error: any) {
	if (error.syscall !== 'listen') {
		throw error;
	}

	const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

	// handle specific listen errors with friendly messages
	switch (error.code) {
		case 'EACCES':
			console.error(`${bind} requires elevated privileges`);
			process.exit(1);
		case 'EADDRINUSE':
			console.error(`${bind} is already in use`);
			process.exit(1);
		default:
			throw error;
	}
}

// Event listener for HTTP server "listening" event
function onListening() {
	const address = server.address();
	const bind = typeof address === 'string' ? `pipe ${address}` : `port ${address?.port}`;

	debug(`Listening on ${bind}...`);
	console.log(`Listening on ${bind}...`);
}
