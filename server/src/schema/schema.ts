import {
	GraphQLSchema,
	GraphQLObjectType,
	GraphQLString,
	GraphQLList,
	GraphQLInt,
	GraphQLID
} from 'graphql';
import _ from 'lodash';
import { IBook, IAuthor } from '../interfaces';
import Book from '../models/book';
import Author from '../models/author';

const AuthorType: any = new GraphQLObjectType<IAuthor>({
	name: 'Author',
	fields: () => ({
		id: { type: GraphQLID },
		name: { type: GraphQLString },
		age: { type: GraphQLInt },
		books: {
			type: new GraphQLList(BookType),
			resolve(author: IAuthor, args) {
				//return _.filter(books, { authorId: author.id });
			}
		}
	})
});
const BookType: any = new GraphQLObjectType<IBook>({
	name: 'Book',
	fields: () => ({
		id: { type: GraphQLID },
		name: { type: GraphQLString },
		genre: { type: GraphQLString },
		author: {
			type: AuthorType,
			resolve(book, args) {
				//return _.find(authors, { id: book.authorId });
			}
		}
	})
});

const RootQuery = new GraphQLObjectType({
	name: 'RootQueryType',
	fields: () => ({
		book: {
			type: BookType,
			args: { id: { type: GraphQLID } },
			resolve(parent, args) {
				// code to get data from db / other source
				// return _.find(books, { id: args.id });
			}
		},
		author: {
			type: AuthorType,
			args: { id: { type: GraphQLID } },
			resolve(parent, args) {
				// return _.find(authors, { id: args.id });
			}
		},
		books: {
			type: new GraphQLList(BookType),
			resolve() {
				// return books;
			}
		},
		authors: {
			type: new GraphQLList(AuthorType),
			resolve() {
				// return authors;
			}
		}
	})
});

const schema = new GraphQLSchema({
	query: RootQuery
});

export default schema;
